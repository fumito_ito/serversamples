'use strict';

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var config = require('./config/config'),
    udp = require('./protocols/udp'),
    tcp = require('./protocols/tcp'),
    wss = require('./protocols/ws');

var timer = setInterval(function () {
  var message = new Buffer(JSON.stringify(config.data)),
      address = udp.address();
  udp.send(message, 0, message.length, address.port, address.address, function (err, bytes) {
  });
}, 1000);

