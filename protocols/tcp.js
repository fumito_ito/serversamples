'use strict';

var net = require('net'),
    config = require('../config/config'),
    server = net.createServer(function (socket) {
      socket.write("{text: 'sample'}\r\n");
      socket.pipe(socket);
    });

server.on('error', function (err) {
  console.log('TCP server is stopped with following error ::' + err);
  server.close();
});

server.on('listening', function () {
  var address = server.address();
  console.log('TCP server is now listening ' + address.address + ':' + address.port);
});

// if connect to this server, send message every seconds from server.
server.on('connection', function (socket) {
  console.log('start connection on TCP server ' + socket.remoteAddress + ':' + socket.remotePort);

  var timer = setInterval(function () {
    console.log('send tcp message');
    if (socket.writable) {
      socket.write(JSON.stringify(config.data) + "\r\n");
    }
  }, 1000);

  socket.on('end', function () {
    clearInterval(timer);
  });
});

server.listen(config.tcp.port, config.host);

module.exports = server;
