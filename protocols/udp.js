'use strict';

var config = require('../config/config'),
    dgram = require('dgram'),
    socket = dgram.createSocket('udp4');

// listeners 
socket.on('error', function (err) {
  console.log('server error:\n', err.stack);
  socket.close();
});

// socket.on('message', function (msg, rinfo) {
//   console.log('got message from ' + rinfo.address + ':' + rinfo.port);
//   console.log('data len: ' + rinfo.size + ' data: ' + msg.toString('ascii', 0, rinfo.size));
// });

socket.on('listening', function () {
  var address = socket.address();
  console.log('UDP server is now listening ' + address.address + ':' + address.port);
});

// configuration
socket.bind(config.udp.port, config.host);

module.exports = socket;
