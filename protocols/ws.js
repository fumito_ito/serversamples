'use strict';

var config = require('../config/config'),
    WebSocketServer = require('ws').Server,
    wss = new WebSocketServer({ port: config.ws.port });

wss.on('listening', function () {
  console.log('WebSocket server is now listening ' + config.host + ':' + config.ws.port); 
});

wss.on('connection', function (ws) {
  console.log('start sending message');

  var timer = setInterval(function () {
    ws.send(JSON.stringify(config.data));
  }, 1000);

  ws.on('close', function () {
    console.log('stop sending message');
    clearInterval(timer);
  });
});

module.exports = wss;
